require('dotenv').config();
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const { isValidUrl } = require('./url-validator');
const { shortenUrl, getOriginalUrl } = require('./short-url-service');

const app = express();

// Basic Configuration
const port = process.env.PORT || 3000;

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/public', express.static(`${process.cwd()}/public`));

app.get('/', function (req, res) {
  res.sendFile(process.cwd() + '/views/index.html');
});

app.post('/api/shorturl', async function (req, res) {
  const url = req.body.url;
  const isUrlValid = await isValidUrl(url);
  if (isUrlValid) {
    const shortenedUrl = await shortenUrl(url);
    if (shortenedUrl !== null) {
      res.json({
        original_url: url,
        short_url: shortenedUrl
      });
    } else {
      res.json({ error: 'unable to handle the request at the moment' });
    }
  } else {
    res.json({ error: 'invalid url' });
  }
});

app.get('/api/shorturl/:shortUrl', function(req, res, next) {
  const shortUrl = new Number(req.params.shortUrl);
  if(!Number.isNaN(shortUrl) && shortUrl > 0) {
    next();
  } else {
    res.json({ error: 'invalid url' });
  }
}, async function (req, res) {
  const originalUrl = await getOriginalUrl(req.params.shortUrl);
  if(originalUrl !== null) {
    res.redirect(originalUrl);
  } else {
    res.json({ error: 'invalid url' });
  }
});

// Your first API endpoint
app.get('/api/hello', function (req, res) {
  res.json({ greeting: 'hello API' });
});

app.listen(port, function () {
  console.log(`Listening on port ${port}`);
});
