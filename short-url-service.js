const { ShortUrl } = require('./short-url-model');

async function shortenUrl(originalUrl) {
    let shortenedUrl = null;
    try {
        const findShortenedUrl = await ShortUrl.findOne({ originalUrl }).exec();
        if (findShortenedUrl === null) {
            const totalDocuments = await ShortUrl.countDocuments();
            const shortUrlToSave = new ShortUrl({ originalUrl, shortUrl: totalDocuments + 1 });
            const savedShortUrl = await shortUrlToSave.save();
            shortenedUrl = savedShortUrl.shortUrl;
        } else {
            shortenedUrl = findShortenedUrl.shortUrl;
        }
    } catch (err) {
        console.log(err);
    }
    return shortenedUrl;
}

async function getOriginalUrl(shortUrl) {
    let originalUrl = null;
    try {
        const findUrl = await ShortUrl.findOne({ shortUrl }).exec();
        originalUrl = findUrl !== null ? findUrl.originalUrl : null;
    } catch(err) {
        console.log(err);
    }
    return originalUrl;
}

module.exports.shortenUrl = shortenUrl;
module.exports.getOriginalUrl = getOriginalUrl;