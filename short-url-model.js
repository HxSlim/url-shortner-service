require('dotenv').config();

const mongoose = require('mongoose');
mongoose.connect(process.env.MONGO_URI);

const shortURLSchema = mongoose.Schema({
    originalUrl: String,
    shortUrl: Number
});

module.exports.ShortUrl = mongoose.model('ShortUrl', shortURLSchema);