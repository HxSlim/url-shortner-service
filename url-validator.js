const dns = require('node:dns');
const promisify = require('node:util').promisify;
const dnsLookup = promisify(dns.lookup);

async function isValidUrl(url) {
    let isValid = true;
    try {
        const parsedURL = new URL(url);
        await dnsLookup(parsedURL.hostname);
    } catch (error) {
        isValid = false;
    }
    return isValid;
}

module.exports.isValidUrl = isValidUrl;